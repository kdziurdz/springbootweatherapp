<%@ page  import="com.dziurdz.model.WeatherState" %>
<%@ page  import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %><%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Pragma","no-cache");
response.setHeader("Expires","0"); %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang=pl>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>KRK Weather</title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
<body>
    <%
        boolean fail = Boolean.parseBoolean(request.getParameter("fail"));
        if(fail){
            response.setIntHeader("Refresh", 3);
        }
         response.setIntHeader("Refresh", 60);
         List<WeatherState> list = new ArrayList<WeatherState>();
         list = (List<WeatherState>)request.getAttribute("list");
         WeatherState actualWeatherState = list.get(list.size()-1);


    %>
    <c:choose>
        <c:when test="${!fail}">
	<div class="jumbotron">
		<h1>Cracow weather</h1>
	</div>
	<div class="container" style="margin-top:50px">

	  	  <div class="row">
      	    <div class="col-md-2">
      	      <img class="img-responsive" src="http://www.bwkrakow.pl/nablogu/wp-content/uploads/2014/09/krakow2.jpg" alt="herb" width="200" />
      	    </div>
      	    <div class="col-md-5">
      				<table class="table table-hover table-striped">
      				    <tbody>
      				          <tr>
                              	<% out.print("<td>"+ actualWeatherState.getWeatherMain() + "</td>"); %>
                              	<% out.print("<td>"+ actualWeatherState.getWeatherDescription() + "</td>"); %>
                              </tr>
                              <tr>
                                <td>Temp</td>
                                <% out.print("<td>"+ actualWeatherState.getMainTemp() +" &deg;C </td>"); %>
                              </tr>
                              <tr>
                                <td>Pressure</td>
                                <% out.print("<td>"+ actualWeatherState.getMainPressure() + " hPa </td>"); %>
                              </tr>
                    			<tr>
      								<td>Humidity</td>
      								<% out.print("<td>"+ actualWeatherState.getMainHumidity() + " % </td>"); %>
      							</tr>
      							 <tr>
                                    <td>Weather time</td>
                                   <% out.print("<td>"+ actualWeatherState.getFormattedDownloadTime() + " </td>"); %>
                                </tr>
      				    </tbody>
      				  </table>
      	    </div>
      	    <div class="col-md-5">
      				<table class="table table-hover table-striped">
      						<tbody>
      							<tr>
      								<td>Wind speed</td>
      								<% out.print("<td>"+ actualWeatherState.getWindSpeed() + " m/s </td>"); %>
      							</tr>
      							<tr>
      								<td>Wind degree</td>
      								<% out.print("<td>"+ actualWeatherState.getWindDegree() + " &deg; </td>"); %>
      							</tr>
      							<tr>
      								<td>Cloudiness</td>
      								<% out.print("<td>"+ actualWeatherState.getCloudsAll() + " % </td>"); %>
      							</tr>
      							<tr>
      								<td>Sunrise</td>
      								<% out.print("<td>"+ actualWeatherState.getSysSunrise() + " </td>"); %>
      							</tr>
      							<tr>
      								<td>Sunset</td>
      								<% out.print("<td>"+ actualWeatherState.getSysSunset() + " </td>"); %>
      							</tr>
      						</tbody>
      					</table>
              </div>
      	  </div>
      	  <c:choose>
      	    <c:when test="${list.size()>1}">
      	  		<div class="row">
          			<div class="col-md-12">
          				<h2>History data</h2>
          			  <p>Table representing last weather states.</p>
          			  <table class="table table-striped">
          			    <thead>
          			      <tr>
          			        <th>Date</th>
          			        <th>Description</th>
          			        <th>Pressure</th>
          					<th>Humidity</th>
          					<th>Temp</th>
          					<th>Wind Speed</th>
         					<th>Degree</th>
          					<th>Cloudiness</th>
          			      </tr>
          			    </thead>
          			    <tbody>
          			        <%

          			            for(int i = list.size()-2; i>=0; i--){
          			                out.println("<tr>");
          			                out.print("<td>"+ list.get(i).getFormattedDownloadTime() + " </td>");
          			                out.print("<td>"+ list.get(i).getWeatherMain() + "</td>");
          			                out.print("<td>"+ list.get(i).getMainPressure() + " hPa </td>");
          			                out.print("<td>"+ list.get(i).getMainHumidity() + " % </td>");
          			                out.print("<td>"+ list.get(i).getMainTemp() +" &deg;C </td>");
          			                out.print("<td>"+ list.get(i).getWindSpeed() + " m/s </td>");
          			                out.print("<td>"+ list.get(i).getWindDegree() + " &deg;</td>");
          			                out.print("<td>"+ list.get(i).getCloudsAll() + " % </td>");
          			                out.println("</tr>");
          			            }
          			        %>
          			        <tr>
          			            <td
          			        </tr>
                        </tbody>
                       </table>

          			</div>

          		</div>
      	    </c:when>
            <c:otherwise>
               <h2>No previous data</h2>
            </c:otherwise>
      	  </c:choose>

	</div>
	</c:when>
    <c:otherwise>
        <h2>DATA FAIL</h2>
    </c:otherwise>
    </c:choose>



</body>
</html>
