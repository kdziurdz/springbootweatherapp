package com.dziurdz;

import com.dziurdz.model.WeatherState;
import com.dziurdz.repository.WeatherStateRepositoryDAO;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JspDemoApplicationTests {


	String resourceUrl = "http://api.openweathermap.org/data/2.5/weather?id=3094802&APPID=ea793bad315b7835b392fcebd30cd2aa&units=metric";

	@Autowired
	WeatherStateRepositoryDAO weatherStateRepositoryDAO;

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void restTemplateRequestTest() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}


	@Test
	public void weatherStateRepositoryTest() {
		WeatherState ws1 = new WeatherState();
		WeatherState ws2 = new WeatherState();

		int sizeBefore = weatherStateRepositoryDAO.findAll().size();
		weatherStateRepositoryDAO.save(ws1);
		weatherStateRepositoryDAO.save(ws2);
		int sizeAfterAdd = weatherStateRepositoryDAO.findAll().size();
		weatherStateRepositoryDAO.deleteAll();
		int sizeAfterDelete = weatherStateRepositoryDAO.findAll().size();

		assertTrue(sizeBefore == 0);
		assertTrue(sizeAfterAdd == 2);
		assertTrue(sizeAfterDelete == 0);
	}

	@Test
	public void clearingJsonTest(){
		boolean validation ;
		String jsonInvalid = "{\"coord\":{\"lon\":19.92,\"";
		String jsonValid = "{\"lon\":19.92,\"lat\":50.08}";

		int firstCut = jsonInvalid.indexOf('{');
		int endCut = jsonInvalid.lastIndexOf('}');
		if(firstCut==-1 || endCut==-1){
			validation = false;
		}
		else {
			validation = true;
		}
		Assert.assertFalse(validation);

		validation = false;
		firstCut = jsonValid.indexOf('{');
		endCut = jsonValid.lastIndexOf('}');
		if(firstCut==-1 || endCut==-1){
			validation = false;
		}
		else {
			validation = true;
		}
		Assert.assertTrue(validation);

	}

}
