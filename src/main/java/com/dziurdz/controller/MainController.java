
package com.dziurdz.controller;

import com.dziurdz.repository.WeatherStateRepositoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by konrad on 12.11.16.
 */

@Controller
public class MainController {

	@Autowired
	private WeatherStateRepositoryDAO weatherStateRepositoryDAO;


	@RequestMapping("/")
	public String mainPage(Model model) {
		if(weatherStateRepositoryDAO.findAll().size()>0){
			model.addAttribute("list", weatherStateRepositoryDAO.findAll());
			return "index";
		}
		else {
			model.addAttribute("fail", true);
			return "index";
		}

	}

}
