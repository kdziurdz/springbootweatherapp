package com.dziurdz.service;

import com.dziurdz.exception.MyJsonParsingException;
import com.dziurdz.model.WeatherState;
import com.dziurdz.repository.WeatherStateRepositoryDAO;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.xml.ws.http.HTTPException;
import java.io.IOException;

/**
 * Created by konrad on 12.11.16.
 */
@Service("jsonWeatherDataHandlingService")
public class JsonWeatherDataHandlingService {

    @Autowired
    WeatherStateRepositoryDAO weatherStateRepositoryDAO;

    @Value("${weather.api.uri}")
    String resourceUrl;


    private String json;

    @Scheduled(initialDelay=1000,fixedRateString="${fixed.rate.miliseconds}")
    public void fixedRateProcess() {
        try{
            sendRequest();
            clearJsonResponse();
            if(!isValidJSON(json)){
                throw new MyJsonParsingException("Json is not valid");
            }
            weatherStateRepositoryDAO.save(parse());
        } catch ( HTTPException | ArrayIndexOutOfBoundsException |
                MyJsonParsingException |IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean isValidJSON(final String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        boolean valid = true;
        try{
            objectMapper.readTree(json);
        } catch(JsonProcessingException e){
            valid = false;
        }
        return valid;
    }

    private void clearJsonResponse() throws MyJsonParsingException{
        int firstCut = json.indexOf('{');
        int endCut = json.lastIndexOf('}');
        if(firstCut==-1 || endCut==-1) throw new MyJsonParsingException("Cannot find brackets");

        json = json.substring(firstCut,endCut);
    }



    private void sendRequest() throws RestClientException, HTTPException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl,String.class);
        json = response.toString();
    }

    public WeatherState parse() throws JsonParseException {

        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        JsonNode root = null;

        try {
            root = mapper.readValue(json, JsonNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        WeatherState ws = new WeatherState();
        JsonNode coord = root.get("coord");
        ws.setCoordLat(root.path("coord").path("lat").asDouble());
        ws.setCoordLon(root.path("coord").path("lon").asDouble());

        try {
            ws.setWeatherId(root.path("weather").get(0).path("id").asInt());
            ws.setWeatherMain(root.path("weather").get(0).path("main").asText());
            ws.setWeatherDescription(root.path("weather").get(0).path("description").asText());
            ws.setIcon(root.path("weather").get(0).path("icon").asText());

            ws.setMainTemp((float) root.path("main").path("temp").asDouble());
            ws.setMainPressure((float) root.path("main").path("pressure").asDouble());
            ws.setMainHumidity((float) root.path("main").path("humidity").asDouble());
            ws.setMainTemp_min((float) root.path("main").path("temp_min").asDouble());
            ws.setMainTemp_max((float) root.path("main").path("temp_max").asDouble());

            ws.setVisibility((float) root.path("visibility").asDouble());

            ws.setWindSpeed((float) root.path("wind").path("speed").asDouble());
            ws.setWindDegree((float) root.path("wind").path("deg").asDouble());

            ws.setCloudsAll((float) root.path("clouds").path("all").asDouble());

            ws.setDaytime(root.path("dt").asLong());

            ws.setSysType(root.path("sys").path("type").asInt());
            ws.setSysId(root.path("sys").path("id").asInt());
            ws.setSysMessage((float) root.path("sys").path("message").asDouble());
            ws.setSysSunrise(root.path("sys").path("sunrise").asText());
            ws.setSysSunset(root.path("sys").path("sunset").asText());
            ws.setSysCountry(root.path("sys").path("country").asText());

            ws.setMainId(root.path("id").asInt());
            ws.setMainCod(root.path("cod").asInt());
            ws.setMainName(root.path("name").asText());
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        return ws;
    }


}
