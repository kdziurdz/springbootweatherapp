package com.dziurdz.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by konrad on 13.11.16.
 */
@Configuration
@EnableAsync
@EnableScheduling
public class AppConfig {
}
