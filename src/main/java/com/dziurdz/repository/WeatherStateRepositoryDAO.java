package com.dziurdz.repository;

import com.dziurdz.model.WeatherState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by konrad on 12.11.16.
 */
@Repository
public interface WeatherStateRepositoryDAO extends JpaRepository<WeatherState, Integer> {
}
