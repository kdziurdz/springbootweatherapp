package com.dziurdz.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

/**
 * Created by konrad on 12.11.16.
 */
@Entity
public class WeatherState {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    private float visibility;

    private long daytime;

    private double coordLon;
    private double coordLat;

    private int weatherId;
    private String weatherMain;
    private String weatherDescription;
    private String icon;

    private float mainTemp;
    private float mainPressure;
    private float mainHumidity;
    private float mainTemp_min;
    private float mainTemp_max;

    private float windSpeed;
    private float windDegree;

    private float cloudsAll;

    private float sysType;
    private int sysId;
    private float sysMessage;
    private String sysSunrise;
    private String sysSunset;
    private String country;
    private LocalDateTime dataDownloadTime = LocalDateTime.now();

    public String getFormattedDownloadTime(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return  dataDownloadTime.format(formatter);
    }


    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setSysCountry(String country) {
        this.country = country;
    }

    private int mainId;
    private String mainName;
    private int mainCod;

    public LocalDateTime getDataDownloadTime() {
        return dataDownloadTime;
    }

    public void setDataDownloadTime(LocalDateTime dataDownloadTime) {
        this.dataDownloadTime = dataDownloadTime;
    }

    public String getSysSunrise() {

        return sysSunrise;
    }

    public void setSysSunrise(String sysSunrise) {
        this.sysSunrise = getFormattedDataStringFromMillis(sysSunrise,"dd-MM-yyyy HH:mm:ss");
    }

    public String getSysSunset() {
        return sysSunset;
    }

    public void setSysSunset(String sysSunset) {

        this.sysSunset = getFormattedDataStringFromMillis(sysSunset,"dd-MM-yyyy HH:mm:ss");
    }

    private String getFormattedDataStringFromMillis(String millis, String format){

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(millis)*1000);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

        return  LocalDateTime.ofInstant(calendar.toInstant(), ZoneId.systemDefault()).format(formatter);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getVisibility() {
        return visibility;
    }

    public void setVisibility(float visibility) {
        this.visibility = visibility;
    }

    public long getDaytime() {
        return daytime;
    }

    public void setDaytime(long daytime) {
        this.daytime = daytime;
    }

    public double getCoordLon() {
        return coordLon;
    }

    public void setCoordLon(double coordLon) {
        this.coordLon = coordLon;
    }

    public double getCoordLat() {
        return coordLat;
    }

    public void setCoordLat(double coordLat) {
        this.coordLat = coordLat;
    }

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

    public String getWeatherMain() {
        return weatherMain;
    }

    public void setWeatherMain(String weatherMain) {
        this.weatherMain = weatherMain;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public float getMainTemp() {
        return mainTemp;
    }

    public void setMainTemp(float mainTemp) {
        this.mainTemp = mainTemp;
    }

    public float getMainPressure() {
        return mainPressure;
    }

    public void setMainPressure(float mainPressure) {
        this.mainPressure = mainPressure;
    }

    public float getMainHumidity() {
        return mainHumidity;
    }

    public void setMainHumidity(float mainHumidity) {
        this.mainHumidity = mainHumidity;
    }

    public float getMainTemp_min() {
        return mainTemp_min;
    }

    public void setMainTemp_min(float mainTemp_min) {
        this.mainTemp_min = mainTemp_min;
    }

    public float getMainTemp_max() {
        return mainTemp_max;
    }

    public void setMainTemp_max(float mainTemp_max) {
        this.mainTemp_max = mainTemp_max;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    public float getWindDegree() {
        return windDegree;
    }

    public void setWindDegree(float windDegree) {
        this.windDegree = windDegree;
    }

    public float getCloudsAll() {
        return cloudsAll;
    }

    public void setCloudsAll(float cloudsAll) {
        this.cloudsAll = cloudsAll;
    }

    public float getSysType() {
        return sysType;
    }

    public void setSysType(float sysType) {
        this.sysType = sysType;
    }

    public int getSysId() {
        return sysId;
    }

    public void setSysId(int sysId) {
        this.sysId = sysId;
    }

    public float getSysMessage() {
        return sysMessage;
    }

    public void setSysMessage(float sysMessage) {
        this.sysMessage = sysMessage;
    }

    public int getMainId() {
        return mainId;
    }

    public void setMainId(int mainId) {
        this.mainId = mainId;
    }

    public String getMainName() {
        return mainName;
    }

    public void setMainName(String mainName) {
        this.mainName = mainName;
    }

    public int getMainCod() {
        return mainCod;
    }

    public void setMainCod(int mainCod) {
        this.mainCod = mainCod;
    }

}
