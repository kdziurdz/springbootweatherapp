package com.dziurdz.exception;

/**
 * Created by konrad on 14.11.16.
 */
public class MyJsonParsingException extends Exception {
    public MyJsonParsingException(String message) {
        super(message);
    }
}
